﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Namespace02
{
    public interface Interface01
    {
        string Operation1(string object2);
    }
    public class Module01 : Interface01
    {
        public string Object1 { get; set; }
      

        public string Operation1(string Object2)
        {
            Object1 = Object2;
            return "Nama Anda Adalah " + Object1;
        }
    }
}


