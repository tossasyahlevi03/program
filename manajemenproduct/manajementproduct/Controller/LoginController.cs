﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using manajementproduct.Service;
using System.Security.Cryptography.X509Certificates;
using manajementproduct.Model;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace manajementproduct.Controller
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        ServiceLogin loginservice = new ServiceLogin();
        UserService tokenx = new UserService();
        BacaToken decriptortoken = new BacaToken();
        [HttpPost]
        [ActionName("Login")]
        public string GetLogin(User us)
        {
            string data = loginservice.login(us.username, us.password);
            
             if(data=="0")
            {
                return "tidak ditemukan";
            }
             else
            {
                string token = tokenx.jsontoken(data);
                return token;
            }



        }




        public string getrole()
        { 
            string authHeader = HttpContext.Request.Headers["Authorization"];
            string encodes = authHeader.Substring("Bearer ".Length).Trim();
            //int start = encodes.IndexOf(".") + 1;
            //int end = encodes.LastIndexOf(".");
            //string result = encodes.Substring(start, end - start);
            var handler = new JwtSecurityTokenHandler();

           

            var token = handler.ReadJwtToken(encodes);

          


            return token.Payload.First().Value.ToString();
            // string x = encodes.Substring(7).Trim();
            //// var token = new JwtSecurityToken(jwtEncodedString: x);
            // var q = token.Claims.First(c => c.Type == "roles").Value;
            //var rs =  decriptortoken.GetPrincipal(encodes);
            //return rs;
        }

        [HttpPost]
        [ActionName("cobarole")]
public string GetRoles()
        {

            return getrole();
        }

    }
}