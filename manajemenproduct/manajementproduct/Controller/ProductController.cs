﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using manajementproduct.Model;
using manajementproduct.Service;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json;
namespace manajementproduct.Controller
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        BacaToken bt = new BacaToken();
        public string getrole()
        {
            string authHeader = HttpContext.Request.Headers["Authorization"];
            string encodes = authHeader.Substring("Bearer ".Length).Trim();
            var rs = bt.GetPrincipal(encodes);
            return rs;
        }

        public string AmbilRole()
        {
            string authHeader = HttpContext.Request.Headers["Authorization"];
            string encodes = authHeader.Substring("Bearer ".Length).Trim();
           
            var handler = new JwtSecurityTokenHandler();



            var token = handler.ReadJwtToken(encodes);




            return token.Payload.First().Value.ToString();
           
        }

        ServiceProduct productservice = new ServiceProduct();
        [HttpPost]
        [Produces("application/json")]
        [ActionName("tambahproduct")]
        public string tambahproduct(product p)
        {
            string role = AmbilRole();
            if (role =="purchasing")
            {
                string hasil = productservice.tambahproduct(p.idproduct, p.namaproduct, p.harga);

                return hasil;
            }
            else
            {
                return "NOT AUTHORIZED";
            }
        }

        [HttpPost]
        [Produces("application/json")]

        [ActionName("updateproduct")]
        public string updateproduct(product p)
        {
            string role = AmbilRole();
            if (role == "administrator" || role=="purchasing")
            {
                string hasil = productservice.updateproduct(p.idproduct, p.namaproduct, p.harga);
                return hasil;
            }
            else
            {
                return "NOT AUTHORIZED";
            }
        }

        [HttpPost]
        [ActionName("hapusproduct")]
        [Produces("application/json")]

        public string hapusproduct(product p)
        {
            string role = AmbilRole();
            if (role == "administrator")
            {
                string hasil = productservice.hapusproduct(p.idproduct);
                return hasil;
            }
            else
            {
                return "NOT AUTHORIZED";
            }
        }

    }
}