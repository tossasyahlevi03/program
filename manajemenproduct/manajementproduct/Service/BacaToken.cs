﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace manajementproduct.Service
{
    public class BacaToken
    {
        public string GetRole(string token)
        {
            var stream = token;
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(stream);
            var tokenS = handler.ReadToken(stream) as JwtSecurityToken;
            var jti = tokenS.Claims.First(claim => claim.Type == "roles").Value;
            return jti;
        }
        string Base64Decode(string value)
        {
            return Encoding.Default.GetString(Convert.FromBase64String(value));
        }


        public string readtoken(string token)
        {
            var jwtHandler = new JwtSecurityTokenHandler();
            var jwtOutput = string.Empty;

            // Check Token Format
            if (!jwtHandler.CanReadToken(token)) throw new Exception("The token doesn't seem to be in a proper JWT format.");


            var tokens = jwtHandler.ReadJwtToken(token);

            // Re-serialize the Token Headers to just Key and Values
            var jwtHeader = JsonConvert.SerializeObject(tokens.Header.Select(h => new { h.Key, h.Value }));
            jwtOutput = $"{{\r\n\"Header\":\r\n{JToken.Parse(jwtHeader)},";

            // Re-serialize the Token Claims to just Type and Values
            var jwtPayload = JsonConvert.SerializeObject(tokens.Claims.Select(c => new { c.Type, c.Value }));
            jwtOutput += $"\r\n\"Payload\":\r\n{JToken.Parse(jwtPayload)}\r\n}}";

            // Output the whole thing to pretty Json object formatted.
            return JToken.Parse(jwtOutput).ToString(Formatting.Indented);
        }

        public string getname(string token)
        {
            string secret = "HWcQ1UYkOSYgBh10xWLMBCLEPw6R3549rAxV7LgfRk81";
            var key = Encoding.ASCII.GetBytes(secret);
            var handler = new JwtSecurityTokenHandler();
            var validation = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false
            };
            var claims = handler.ValidateToken(token, validation, out var tokensecure);
            return claims.Identity.Name;
        }
        public string get(string token)
        {
            var stream = token;
            var handler = new JwtSecurityTokenHandler();
            var jsontoken = handler.ReadToken(stream);
            var tokenS = handler.ReadToken(stream) as JwtSecurityToken;

            var jti = tokenS.Claims.First(claim => claim.Type == "jti").Value;
            return jti;
        }
        public string GetPrincipal(string token)
        {
            try
            {
                string x = token;

                var symmetricKey = Convert.FromBase64String("HWcQ1UYkOSYgBh10xWLMBCLEPw6R3549rAxV7LgfRk81");
                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey),
                   
                };

                var handler = new JwtSecurityTokenHandler();
                handler.InboundClaimTypeMap.Clear();

                SecurityToken securityToken;


                var principal = handler.ValidateToken(x, validationParameters, out securityToken);




                var q = principal.Identity.Name;
                return q;
            }

            catch (Exception ex)
            {
                return ex.Message;
            }
        }

       
        public string bacatoken2(string token1)
        {
            // string tokenString = token.PadRight(token.Length + (4 - token.Length % 4) % 4, '=');


            //  string authToken ="6052fqw89034tjf9f8q2bfj0a23nf0creyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0aW1lc3RhbXBJblNlY29uZHMiOiIxNTk1ODM4OTE2IiwidHJhbnNhY3Rpb25JZCI6Ijk1ODM4OTE2In0.d2fT6FRYDSQ1m_ou9Qerg4vcXtdwOUYE_xVCUisS5iE";

            //   string x1 = token1.Substring(32);

            string x = token1;
          //  var token = new System.IdentityModel..JwtSecurityToken(x);

            var tokenHandler = new JwtSecurityTokenHandler();
                    var validationParameters = GetValidationParameters();

                    SecurityToken validatedToken;
                    IPrincipal principal = tokenHandler.ValidateToken(x, validationParameters, out validatedToken);
            return principal.Identity.Name;

           // return true;
               
        }

        public TokenValidationParameters GetValidationParameters()
        {
            string key = "HWcQ1UYkOSYgBh10xWLMBCLEPw6R3549rAxV7LgfRk81";
            return new TokenValidationParameters()
            {
                ValidateLifetime = false, // Because there is no expiration in the generated token
                ValidateAudience = false, // Because there is no audiance in the generated token
                ValidateIssuer = false,   // Because there is no issuer in the generated token
                ValidIssuer = "BMI",
                ValidAudience = "BMI",
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)) // The same key as the one that generate the token
            };
        }

        public string tesuserid(string token)
        {
            string secret = "HWcQ1UYkOSYgBh10xWLMBCLEPw6R3549rAxV7LgfRk81";
            var s = Convert.FromBase64String(secret);
            var key = s;
            string o = Base64Decode(token);
           
               // string x1 = Encoding.Default.GetString(Convert.FromBase64String(token));
            


            var handler = new JwtSecurityTokenHandler();
            var validations = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret)),

                ValidateIssuer = false,
                ValidateAudience = false,


            };
            var claims = handler.ValidateToken(token, validations, out var tokenSecure);
            return claims.Identity.Name;
        }

    }
}
