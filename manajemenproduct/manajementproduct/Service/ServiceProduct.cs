﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace manajementproduct.Service
{
    public class ServiceProduct
    {


        public string updateproduct(string idproduct, string namaproduct, int harga)
        {
            try
            {
                SqlConnection scon = new SqlConnection(stringkoneksi.connection);

                string masukdata = "update product set namaproduct =@namaproduct, harga=@harga  where idproduct='" + idproduct + "'";

                SqlCommand scom = new SqlCommand(masukdata, scon);
                scom.Parameters.Add(new SqlParameter("namaproduct", namaproduct));
                scom.Parameters.Add(new SqlParameter("harga", harga));


                scon.Open();
                scom.ExecuteNonQuery();

                scon.Close();
                return "ok";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string hapusproduct(string idproduct)
        {
            try
            {
                SqlConnection scon = new SqlConnection(stringkoneksi.connection);

                string masukdata = "delete from product  where idproduct='" + idproduct + "'";

                SqlCommand scom = new SqlCommand(masukdata, scon);


                scon.Open();
                scom.ExecuteNonQuery();

                scon.Close();
                return "ok";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string tambahproduct(string idproduct, string namaproduct, int harga)
        {

            try
            {
                SqlConnection ncon = new SqlConnection(stringkoneksi.connection);
                string masukdata = "insert into product (idproduct,namaproduct,harga) values(@idproduct,@namaproduct,@harga)";
                SqlCommand ncom = new SqlCommand(masukdata, ncon);
                ncom.Parameters.Add(new SqlParameter("idproduct", idproduct));
                ncom.Parameters.Add(new SqlParameter("namaproduct", namaproduct));
                ncom.Parameters.Add(new SqlParameter("harga", harga));

                ncon.Open();
                ncom.ExecuteNonQuery();
                ncon.Close();
                return "ok";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
