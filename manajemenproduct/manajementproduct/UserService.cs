﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace manajementproduct
{
    public class UserService
    {

        
      
            //public AuthenticateResponse Authenticate(AuthenticateRequest model)
            //{
            //    var user = _users.SingleOrDefault(x => x.apiID == model.apiID && x.apiKey== model.apiKey);

            //    // return null if user not found
            //    if (user == null) return null;

            //    // authentication successful so generate jwt token
            //    var token = generateJwtToken(user);

            //    return new AuthenticateResponse(token);
            //}

        


        

            // helper methods
            private readonly Random _random = new Random();

            // Generates a random number within a range.      
            public string RandomNumber(int min, int max)
            {
                return _random.Next(min, max).ToString();
            }



        private readonly AppSetting _appSettings;


        public string jsontoken(string roles)
        {
            string key = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";

           // Create Security key  using private key above:
           // not that latest version of JWT using Microsoft namespace instead of System
            var securityKey = new Microsoft
               .IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));

            // Also note that securityKey length should be >256b
            // so you have to make sure that your private key has a proper length
            //
            var credentials = new Microsoft.IdentityModel.Tokens.SigningCredentials
                              (securityKey, SecurityAlgorithms.HmacSha256Signature);

            //  Finally create a Token
            var header = new JwtHeader(credentials);

            //Some PayLoad that contain information about the  customer
            var payload = new JwtPayload
           {
               { "roles ",roles},
               { "scope", "http://dummy.com/"},
           };

            //
            var secToken = new JwtSecurityToken(header, payload);
            var handler = new JwtSecurityTokenHandler();

            // Token to String so you can use it in your client
            var tokenString = handler.WriteToken(secToken);
            return tokenString;
        }

        public string generateJwtToken(string roles)
            {
                // generate token that is valid for 7 days
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("HWcQ1UYkOSYgBh10xWLMBCLEPw6R3549rAxV7LgfRk81");
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                 new Claim ("Timespaninsecond", DateTime.Now.Millisecond.ToString()),
                  new Claim ("TransactionId", RandomNumber(0,9999999)),
                  new Claim("roles",roles)
                    }),

                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)

                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                return tokenHandler.WriteToken(token);
            }
        }
    }



