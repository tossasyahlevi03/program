﻿using System;

namespace Namespace01

{
    interface interface03
    {
        string operation1(string object2);
    }
    public class Module02 : interface03
    {
        public string object1 { get; set; }
        public string operation1(string object2)
        {
            object1 = object2;
            return "Nama Anda Adalah " + object1;
        }
    }
}


