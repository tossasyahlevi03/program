﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Namespace01
{
   public interface interface01
    {
        string object21 { get; set; }
        string operation1(string object2);
    };

  
    public class Module01 :interface01
    {
        public string object1 { get; set; }
        string interface01.object21 { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public string operation1(string object2)
        {
            object1 = object2;
            return "Nama Anda Adalah " + object1;
        }
    }
}
