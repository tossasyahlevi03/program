﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageCache001.aspx.cs" Inherits="ProgramCache001.PageCache001" %>
<%@ OutputCache Duration="60" VaryByParam="txt1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox runat="server" ID="txt1"></asp:TextBox>
            <asp:Button runat="server" OnClick="btn1_Click" ID="btn1" />
        </div>
    </form>
</body>
</html>
