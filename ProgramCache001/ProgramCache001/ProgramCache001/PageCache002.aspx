﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageCache002.aspx.cs" Inherits="ProgramCache001.PageCache002" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Label ID="lbltime" runat="server"></asp:Label>
            <asp:SqlDataSource ID="s" runat="server" ConnectionString="<%$ ConnectionStrings:db_productConnectionString %>" SelectCommand="SELECT [idproduct], [namaproduct], [harga] FROM [product]">
               </asp:SqlDataSource>

            <asp:GridView runat="server" AutoGenerateColumns="False" DataKeyNames="idproduct" DataSourceID="s">
                <Columns>
                    <asp:BoundField DataField="idproduct" HeaderText="idproduct" ReadOnly="True" SortExpression="idproduct" />
                    <asp:BoundField DataField="namaproduct" HeaderText="namaproduct" SortExpression="namaproduct" />
                    <asp:BoundField DataField="harga" HeaderText="harga" SortExpression="harga" />
                </Columns>
            </asp:GridView>
                        <asp:Label ID="lblinfo" runat="server"></asp:Label>

        </div>
    </form>
</body>
</html>
