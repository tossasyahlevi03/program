﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp11
{
    public class Model1
    {
        public string nama { get; set; }
        public string kota { get; set; }
    }
   public class Modul3
    {
        Model1 model = new Model1();
        public string IsiProfil(string nama, string kota)
        {
            model.nama = nama;
            model.kota = kota;

            if (kota.ToLower() == "semarang")
            {
                return nama+","+ "Jawa Tengah";
            }
            else
            {
                return nama+","+ "Indonesia";
            }

        }
    
    }
}
